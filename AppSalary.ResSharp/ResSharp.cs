﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace AppSalary.ResSharp
{
    public class ResSharp 
    {

        private string UrlBase => AppConfiguration.GetConfiguration<string>(AppKeys.WinFormApp.UrlBase);
        private string Rootbase => AppConfiguration.GetConfiguration<string>(AppKeys.WinFormApp.Rootbase);
        private static HttpClient client
        {
            get;
            set;
        }

        private Uri GetUri(string route)
        {
            if (!string.IsNullOrEmpty(UrlBase))
                return new Uri(new Uri(UrlBase), $"{Rootbase}{route}");

            return new Uri(route);
        }
        private HttpClient Cliente(HttpClientHandler handler, string token = null)
        {
            var authValue = new AuthenticationHeaderValue("Bearer", token);
            if (token != null)
            {
                client = new HttpClient(handler)
                {
                    DefaultRequestHeaders = { Authorization = authValue },

                };
            }
            else
            {
                client = new HttpClient()
                {

                };
            }
            return client;
        }


        public async Task<T> RequestAsync<T>(string route, object data, HttpMethod typepost, bool checkToken = true)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            string tokenData = string.Empty;

            try
            {
                if (checkToken)
                {
                    return await SendRequestAsync<T>(route, content, typepost, new AuthenticationHeaderValue("Bearer", GlobalsVariables.Usertoken));
                }
                else
                {
                    return await SendRequestAsync<T>(route, content, typepost, null);

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

                throw;
            }
        }


        private async Task<T> SendRequestAsync<T>(string route, ByteArrayContent content, HttpMethod httpMethod, AuthenticationHeaderValue authHeader, Dictionary<string, string> headerAttributes = null)
        {
            string contentRes = string.Empty;
            string requestContent = content?.ReadAsStringAsync()?.Result;

            try
            {
                using (var handler = new HttpClientHandler())
                {

                    using (Cliente(handler, Globals.GlobalsVariables.Usertoken))
                    {
                        using (var request = new HttpRequestMessage() { RequestUri = GetUri(route), Method = httpMethod, Content = content })
                        {
                            if (authHeader != null)
                            {
                                request.Headers.Authorization = authHeader;
                            }

                            request.Headers.Add("Accept", "*/*");

                            if (headerAttributes != null)
                            {
                                foreach (var attr in headerAttributes)
                                {
                                    request.Headers.Add(attr.Key, attr.Value);
                                }
                            }

                            using (var res = await client.SendAsync(request))
                            {
                                if (!res.IsSuccessStatusCode)
                                {
                                    MessageBox.Show("error");
                                }


                                string json = await res.Content.ReadAsStringAsync();
                                contentRes = json;
                                if (json != null)
                                {
                                    return JsonConvert.DeserializeObject<T>(json);
                                }
                            }

                            content.Dispose();
                            return default(T);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public async Task<T> Upload<T>(byte[] image, string imagename, HttpClient client, string endpoint)
        {
            using (var handler = new HttpClientHandler())
            {
                using (Cliente(handler, Globals.GlobalsVariables.Usertoken))
                {
                    using (var content =
                        new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                    {
                        content.Add(new StreamContent(new MemoryStream(image)), "file", imagename);

                        using (
                           var message =
                               await client.PostAsync(endpoint, content))
                        {

                            string json = await message.Content.ReadAsStringAsync();
                            if (json != null)
                            {
                                return JsonConvert.DeserializeObject<T>(json);
                            }

                            return default(T);
                        }
                    }
                }
            }
        }
    }
}

using NUnit.Framework;
using Moq;
using AppSalary.Business;
using AppSalary.Core;
using AppSalary.Models;
using AppSalary.Business.Process;
using System.Threading.Tasks;

namespace AppSalary.Test
{
    public class Tests
    {
        Mock<IEmployeeProcess> _transactions;
        private IEmployeeProcess _employeeProcess;
        [SetUp]
        public void Setup()
        {
            _transactions = new Mock<IEmployeeProcess>();
            _employeeProcess = new EmployeeProcess();
        }

        [Test]
        public void TestInfraMoc()
        {
            var seq = new MockSequence();
            _transactions.InSequence(seq).Setup(x => x.GetEmployees(0));

            Assert.Pass();
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(-1)]

        public void TestInfraMoc(int id)
        {
            var seq = new MockSequence();
            _transactions.InSequence(seq).Setup(x => x.GetEmployees(id));

            Assert.Pass();
        }

        [TestCase(-1)]      
        public async Task TestGetEmployeEpty(int Id)
        {
            var result = await _employeeProcess.GetEmployees(Id);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public async Task TestGetEmployees()
        {
            var result = await _employeeProcess.GetEmployees(0);
            Assert.AreEqual(2, result.Count);       
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task TestGetEmploye(int Id)
        {
            var result = await _employeeProcess.GetEmployees(Id);
            Assert.AreEqual(1, result.Count);
        }
    }
}
﻿using Newtonsoft.Json;

namespace AppSalary.Models
{
     public class EmployeeModelDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("contractTypeName")]
        public string ContractTypeName { get; set; }

        [JsonProperty("roleName")]
        public string RoleName { get; set; }


        [JsonProperty("roleId")]
        public int RoleId { get; set; }

        [JsonProperty("annualSalary")]
        public decimal AnnualSalary { get; set; }

        [JsonProperty("hourlySalary")]
        public decimal HourlySalary { get; set; }

        [JsonProperty("monthlySalary")]
        public decimal MonthlySalary { get; set; }


        [JsonProperty("roleDescription")]
        public string RoleDescription { get; set; }

    }

}

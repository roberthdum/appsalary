﻿using AppSalary.Business.Process;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppSalary.Api.Controllers
{
    [ApiController]
    [Route("Api/v1/[controller]")]
    public class EmployeeController : Controller
    {

        private IEmployeeProcess _employeeProcess;

        public EmployeeController(IEmployeeProcess employeeProcess)
        {
            _employeeProcess = employeeProcess;
        }

        [HttpGet]
        public async Task<dynamic> Get([FromQuery] int id)
        {
            return await _employeeProcess.GetEmployees(id);
        }
    }
}

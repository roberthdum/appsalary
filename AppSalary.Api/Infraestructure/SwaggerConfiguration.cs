﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppSalary.Api.Infraestructure
{
    public class SwaggerConfiguration
    {


        public const string EndpointDescription = "Salary Test";


        public const string EndpointUrl = "/swagger/v1/swagger.json";

      
        public const string ContactName = "Roberth Dudiver";

      
        public const string ContactUrl = "http://dudiver.com";

   
        public const string DocNameV1 = "v1";

    
        public const string DocInfoTitle = "Salary Test";

 
        public const string DocInfoVersion = "v1";

        public const string DocInfoDescription = "Salary Test- Roberth Dudiver";
    }
}

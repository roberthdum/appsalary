﻿using AppSalary.Models;
using AppSalary.Web.Models;
using AppSalary.Web.Process;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AppSalary.Web.Controllers
{
    public class EmployeesController : Controller
    {

        public IActionResult Index()
        {
            var Employees = GetEmployees(0);
            return View(Employees);
        }

        private static List<EmployeeModelDTO> GetEmployees(int id)
        {
            var Process = new EmployeeProcess();
            var Employees = Process.GetEmployees(id).Result;
            return Employees;
        }

        [HttpGet]
        public ActionResult GetEmploye(string id)
        {
          
            var rtn = GetEmployees(Convert.ToInt32(id));
            var html = "";
            foreach (var item in rtn)
            {
                 html += $@"<tr>
                <th scope=""row"">{ item.Id}</th>
                 <td>{item.Name}</td>
                 <td> {item.RoleName }</td>

                 <td> {String.Format("{0:C}", item.AnnualSalary)}</td>
                <td>{(item.ContractTypeName == "HourlySalaryEmployee" ? $@"<i class=""far fa-clock""  title=""{item.ContractTypeName}""></i>" : $@"<i class=""fas fa-calendar-alt""  title=""{item.ContractTypeName}""></i>")}</td>
            </tr>";
            }
            return Json(html);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }


}

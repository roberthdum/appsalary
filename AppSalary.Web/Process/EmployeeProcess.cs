﻿using AppSalary.Core.ApiRes;
using AppSalary.Core.Configuration;
using AppSalary.Models;
using AppSalary.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppSalary.Web.Process
{
    public class EmployeeProcess 
    {
        private IResSharp _resSharp;
        private string Employeendpoint => AppConfiguration.GetConfiguration<string>(AppKeys.ResConfigWeb.Employees);
        private string UrlBase => AppConfiguration.GetConfiguration<string>(AppKeys.ResConfigWeb.UrlBase);
        private string Rootbase => AppConfiguration.GetConfiguration<string>(AppKeys.ResConfigWeb.Rootbase);
        public EmployeeProcess()
        {
            _resSharp = new ResSharp(UrlBase, Rootbase);
        }

    
        public async Task<List<EmployeeModelDTO>> GetEmployees(int id = 0)
        {
            var resquesurl = string.Format(Employeendpoint, id);
          

            return await _resSharp.RequestAsync<List<EmployeeModelDTO>>(resquesurl, null, HttpMethod.Get, false);

        }
    }
}

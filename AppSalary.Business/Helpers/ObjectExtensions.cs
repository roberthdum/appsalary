﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace AppSalary.Business
{
    public static class ObjectExtensions
    {
        public static T ToObject<T>(this object obj)
        {
            Type TypeOfClass = typeof(T);
            return (T)Convert.ChangeType(obj, TypeOfClass);
        }
        public static T ToConvertObjects<T>(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static T ToConvertObjects<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}

﻿using AppSalary.Business.ProcesssModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppSalary.Business.Process
{
    public class MonthlySalary : Employee
    {
        public MonthlySalary(int id,
                         string name,
                         string contractTypeName,
                         int roleId,
                         string roleName,
                         string roleDescription,
                         decimal hourlySalary,
                         decimal monthlySalary)
        {
            Id = id;
            Name = name;
            ContractTypeName = contractTypeName;
            RoleId = roleId;
            RoleName = roleName;
         
            RoleDescription = roleName;
            HourlySalary = hourlySalary;
            MonthlySalary = monthlySalary;
        }
        public MonthlySalary()
        {

        }
        public override decimal AnnualSalary
        {
            get
            {

                return MonthlySalary * 12;
            }
       

        }
    }
}

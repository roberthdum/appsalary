﻿using System;
using System.Collections.Generic;
using System.Text;
using AppSalary.Business.ProcesssModel;
namespace AppSalary.Business.Process
{
    public class HourlySalary : Employee
    {
        public HourlySalary(int id,
                      string name,
                      string contractTypeName,
                      int roleId,
                      string roleName,
                      string roleDescription,
                      decimal hourlySalary,
                      decimal monthlySalary)
        {
            Id = id;
            Name = name;
            ContractTypeName = contractTypeName;
            RoleId = roleId;
            RoleName = roleName;
            RoleDescription = roleName;
            HourlySalary = hourlySalary;
            MonthlySalary = monthlySalary;
        }
        public HourlySalary()
        {
        }
        public override decimal AnnualSalary
        {
            get
            {
                return 120 * HourlySalary * 12; ;
            }
     
        }
    }
}

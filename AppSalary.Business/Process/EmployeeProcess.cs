﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using AppSalary.Core.ApiRes;
using AppSalary.Business.ProcesssModel;
using System.Threading.Tasks;

using AppSalary.Core.Configuration;
using AppSalary.Models;
using System.Net.Http;
using AppSalary.Business;
using AppSalary.Core;

namespace AppSalary.Business.Process
{
    public class EmployeeProcess : IEmployeeProcess
    {
        private IResSharp _resSharp;
        private string Employeendpoint => AppConfiguration.GetConfiguration<string>(AppKeys.ResConfig.Employees);

        public EmployeeProcess()
        {
            _resSharp = new ResSharp();
        }


        private List<EmployeeModelDTO> MapEmploye(List<EmployeeModelDTO> employees, int id=0)
        {

            var result = new List<EmployeeModelDTO>();

            if (id != 0)
            {
                employees = employees.Where(x => x.Id == id).ToList();
            }


           
                /*  this is classic factory build method/ uncomment  this and comment in the lines below to test
            Employee employe = null;           
            employe = factoryProcess(employees, result, employe);
        */

           result.AddRange(employees.Where(x =>  x.ContractTypeName.ToLower() == "monthlysalaryemployee").ToConvertObjects<List<MonthlySalary>>().ToConvertObjects<List<EmployeeModelDTO>>());
           result.AddRange(employees.Where(x =>  x.ContractTypeName.ToLower() == "hourlysalaryemployee").ToConvertObjects<List<HourlySalary>>().ToConvertObjects<List<EmployeeModelDTO>>());

            return result.OrderBy(x => x.Id).ToList(); 


        }

        private static Employee factoryProcess(List<EmployeeModelDTO> employees, List<EmployeeModelDTO> result, Employee employe)
        {
            foreach (var item in employees)
            {
                switch (item.ContractTypeName.ToLower())
                {
                    case "hourlysalaryemployee":
                        employe = new HourlySalary(item.Id,
                                    item.Name,
                                    item.ContractTypeName,
                                    item.RoleId,
                                    item.RoleName,
                                    item.RoleDescription,
                                    item.HourlySalary,
                                    item.MonthlySalary);


                        break;
                    case "monthlysalaryemployee":
                        employe = new MonthlySalary(item.Id,
                                    item.Name,
                                    item.ContractTypeName,
                                    item.RoleId,
                                    item.RoleName,
                                    item.RoleDescription,
                                    item.HourlySalary,
                                    item.MonthlySalary);
                        break;
                }

                var employeeDTO = new EmployeeModelDTO();
                employeeDTO.Id = employe.Id;
                employeeDTO.Name = employe.Name;
                employeeDTO.RoleName = employe.RoleName;
                employeeDTO.ContractTypeName = employe.ContractTypeName;
                employeeDTO.AnnualSalary = employe.AnnualSalary;
                result.Add(employeeDTO);


            }

            return employe;
        }

        public async Task<List<EmployeeModelDTO>> GetEmployees(int id = 0)
        {

            return MapEmploye(await _resSharp.RequestAsync<List<EmployeeModelDTO>>(Employeendpoint, null, HttpMethod.Get, false),id);

        }
    }
}

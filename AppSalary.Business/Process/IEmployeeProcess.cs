﻿using AppSalary.Models;
using System.Collections.Generic;

namespace AppSalary.Business.Process
{
    public interface IEmployeeProcess
    {
        System.Threading.Tasks.Task<List<EmployeeModelDTO>> GetEmployees(int id = 0);
    }
}
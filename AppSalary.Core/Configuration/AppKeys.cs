﻿namespace AppSalary.Core.Configuration
{
    public class AppKeys
    {
        public const string Bearer = "Bearer";


        public struct ResConfig
        {
            public const string UrlBase = "ResConfig:urlbase";
            public const string Rootbase = "ResConfig:rootbase";
            public const string Employees = "ResConfig:Employees";

        }
        public struct ResConfigWeb
        {
            public const string UrlBase = "ResConfigWeb:urlbase";
            public const string Rootbase = "ResConfigWeb:rootbase";
            public const string Employees = "ResConfigWeb:Employees";

        }
    }
}
﻿using System.Net.Http;
using System.Threading.Tasks;

namespace AppSalary.Core.ApiRes
{
    public interface IResSharp
    {
        Task<T> RequestAsync<T>(string route, object data, HttpMethod typepost, bool checkToken = true);
        Task<T> Upload<T>(byte[] image, string imagename, HttpClient client, string endpoint);
    }
}